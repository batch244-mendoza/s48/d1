console.log("Hello, World");

// Mock database
let	posts = [];

// Post ID
let count = 1;

// Add Post
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.

document.querySelector("#form-add-post").addEventListener("submit", (event) => {

	event.preventDefault();

	posts.push({
		id : count,
		title : document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	// count will increment everytime a post is created
	count++;
	alert("Succesfully added")
	showPost(posts);
})

// Show posts

const showPost = (posts) => {

	// Variable that will contain all the posts
	let postEntries = "";

	posts.forEach((post) => {

		// We can assign HTML elements in JS Variables
		postEntries += ` <div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')"> Edit </button>
			<button onclick="deletePost('${post.id}')"> Delete </button>

		 </div>`

	})

	// To display the posts in our HTML document
	document.querySelector("#div-post-entries").innerHTML = postEntries;

}

// Edit Post
// This will trigger an event that will update a certain POST upon clicking the edit button.

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

}

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {

		if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {

			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPost(posts);
			alert("Succesfully updated");

			break;

		}


	}


})


// Delete Post

const deletePost = (id) => {
  for (let i = 0; i < posts.length; i++) {
    if (posts[i].id.toString() === id.toString()) {
      posts.splice(i, 1);
    }
  }
  showPost(posts);
  alert(`Post deleted successfully`);
};